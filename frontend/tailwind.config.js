/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        'noto_sans' : ['Noto Sans TC']
      },
      height: {
       132: '37.5rem',
      },
      colors: {
        header: "#fbd07c",
        nav: "#f8f2dd",
        gray_limpid: "rgba(0,0,0,0.12)",
        gray: "#6b7280",
        white: "#FFFFFF",
        background: "#FFFEF0",
      },
    },
  },
  plugins: []
};
