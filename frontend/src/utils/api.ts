interface Activity {
  ID: number;
  ANAME: string;
  A_STIME: string;
  A_ETIME: string;
  R_STIME: string;
  R_ETIME: string;
  DESCRIPT: string;
  LOCAT: string;
  MGR_UEMAIL: string;
  IMAGES: string;
}
interface Participant {
  ID: number;
  RNAME: string;
  UEMAIL: string;
  UNAME: string;
  UPHONE: string;
}
interface ActivityInfo {
  ID: number;
  ANAME: string;
  IMAGE: string;
}
interface HomeActivity {
  ID: number;
  ANAME: string;
  COVER: string;
}
const api = {
  // hostName should be manually adjusted
  hostName: "http://localhost:8000/",
  async googleLogin(payload: any): Promise<any> {
    return await fetch(`${api.hostName}account/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(payload)
    });
  },
  async getParticipants(activityId: string): Promise<any> {
    return await fetch(
      `${api.hostName}api/1.0/activity/participants?activityId=${activityId}`,
      {
        headers: new Headers({
          "Content-Type": "application/json",
          Authorization: `Bearer ${sessionStorage.getItem("token") ?? ""}`
        })
      }
    ).then(async (res) => await res.json());
  },
  async ActivityList(id: string): Promise<Activity> {
    return await fetch(
      `http://localhost:8000/api/1.0/activity?activityId=${id}`,
      {
        headers: new Headers({
          "Content-Type": "application/json",
          Authorization: `Bearer ${sessionStorage.getItem("token") ?? ""}`
        })
      }
    ).then(async (res) => await res.json());
  },
  async ParticipantList(id: string): Promise<Participant[]> {
    return await fetch(
      `http://localhost:8000/api/1.0/activity/participants?activityId=${id}`,
      {
        headers: new Headers({
          "Content-Type": "application/json",
          Authorization: `Bearer ${sessionStorage.getItem("token") ?? ""}`
        })
      }
    ).then(async (res) => await res.json());
  },
  async createRegister(id: string, name: string, phone: string): Promise<any> {
    const requestBody = JSON.stringify({ name, phone });
    return await fetch(
      `${api.hostName}api/1.0/activity/register/create?activityId=${id}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${sessionStorage.getItem("token") ?? ""}`
        },
        body: requestBody
      }
    );
  },
  async createActivity(activity: any): Promise<any> {
    const requestBody = JSON.stringify(activity);
    return await fetch(`${api.hostName}api/1.0/activity/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${sessionStorage.getItem("token") ?? ""}`
      },
      body: requestBody
    });
  },
  async getRegisteredActivitie(): Promise<ActivityInfo[]> {
    return await fetch(`${api.hostName}api/1.0/activity/registered`, {
      headers: new Headers({
        "Content-Type": "application/json",
        Authorization: `Bearer ${sessionStorage.getItem("token") ?? ""}`
      })
    }).then(async (res) => await res.json());
  },
  async getHeldActivities(): Promise<ActivityInfo[]> {
    return await fetch(`${api.hostName}api/1.0/activity/held`, {
      headers: new Headers({
        "Content-Type": "application/json",
        Authorization: `Bearer ${sessionStorage.getItem("token") ?? ""}`
      })
    }).then(async (res) => await res.json());
  },
  async HomepageInfo(): Promise<HomeActivity[]> {
    return await fetch("http://localhost:8000/api/1.0/activity/hots", {
      headers: new Headers({
        "Content-Type": "application/json"
        // Authorization: `Bearer ${jwtToken ?? ""}`
      })
    }).then(async (res) => await res.json());
  }
};

export default api;
