import React, { useEffect, useState } from "react";
import api from "../utils/api";
import Header from "../components/Header";
import { Link } from "react-router-dom";
interface ActivityInfo {
  ID: number;
  ANAME: string;
  IMAGE: string;
}
export default function ParticipantPanel(): JSX.Element {
  const [activities, setActivities] = useState<ActivityInfo[]>();
  useEffect(() => {
    api
      .getRegisteredActivitie()
      .then((res) => {
        const activity = res.map((Info: ActivityInfo) => ({
          ID: Info.ID,
          ANAME: Info.ANAME,
          IMAGE: Info.IMAGE
        }));
        setActivities(activity);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);
  if (activities === undefined) {
    return <div>Wait....</div>;
  }
  return (
    <div>
      <Header />
      <div className="min-h-screen bg-background px-20 py-10 font-noto_sans">
        <div className="text-2xl">我參加的活動</div>
        <hr className="my-4 border-2 border-t border-gray" />
        <div className="grid grid-cols-3 gap-4">
          {activities.map((activity) => (
            <div key={activity.ID}>
              <div className="my-2 flex flex-col items-center">
                <Link to={`/activity/${activity.ID}`} className="">
                  <img src={activity.IMAGE} alt="" className="h-64 w-64" />
                </Link>
                <div className="mt-0 text-center">{activity.ANAME}</div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
