import React from "react";
import Header from "../components/Header";
import Display from "../components/Display";

const Home: React.FC = () => {
  return (
    <div>
      <Header />
      <Display />
    </div>
  );
};

export default Home;
