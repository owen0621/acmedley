import React, { useRef, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import api from "../utils/api";

interface Activity {
  ID: number;
  ANAME: string;
  COVER: string;
}

const Display = (): JSX.Element => {
  const [activities, setActivities] = useState<Activity[]>([]);
  const displayRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    api
      .HomepageInfo()
      .then((res) => {
        setActivities(res);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  return (
    <div ref={displayRef} className="bg-background">
      <div className="flex items-center justify-between px-4 py-2">
        <h2 className="ml-16 font-noto_sans text-lg">Activities</h2>
      </div>
      <div className="ml-16 grid grid-cols-1 gap-4 p-4 font-noto_sans md:grid-cols-3">
        {activities.map((activity) => (
          <Link to={`/activity/${activity.ID}`} key={activity.ID}>
            <div>
              <div className="my-2 text-2xl font-bold">{activity.ANAME}</div>
              <div className="my-2 flex h-96 items-center justify-center">
                <img
                  src={activity.COVER}
                  alt=""
                  className="mx-auto max-h-full max-w-full"
                />
              </div>
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default Display;
