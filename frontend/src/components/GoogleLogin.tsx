import React from "react";
import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";
import jwtDecode from "jwt-decode";
import api from "../utils/api";

function Google(): JSX.Element {
  return (
    <GoogleLogin
      onSuccess={(credentialResponse) => {
        const responsePayload: any = jwtDecode(
          credentialResponse?.credential ?? "error"
        );
        // console.log(responsePayload);
        // FIXME: 需處理 credentialResponse.credential 為 undefine 的情況
        const name = responsePayload.family_name.concat(
          responsePayload.given_name
        );
        const email = responsePayload.email;
        const picture = responsePayload.picture;
        sessionStorage.setItem("picture", picture);
        const payload = {
          user_name: name,
          user_mail: email
        };

        api
          .googleLogin(payload)
          .then((res) => {
            res.json().then((res: any) => {
              console.log(res);
              sessionStorage.setItem("user_name", res.name);
              sessionStorage.setItem("user_mail", res.email);
              sessionStorage.setItem("token", res.token);
              window.location.reload();
            });
          })
          .catch((_) => {
            alert("Login Failed");
          });
        window.location.reload();
      }}
      onError={() => {
        console.log("Login Failed");
      }}
    />
  );
}

function GooLogin(): JSX.Element {
  return (
    <div>
      <GoogleOAuthProvider clientId="491917365539-1a7pdmp5rqikqo190c7fif5hun6dbc27.apps.googleusercontent.com">
        <Google />
      </GoogleOAuthProvider>
    </div>
  );
}

function LogOut(): boolean {
  try {
    sessionStorage.removeItem("user_name");
    sessionStorage.removeItem("user_mail");
    sessionStorage.removeItem("token");
  } catch (err) {
    return false;
  }
  window.location.reload();
  return true;
}
export { GooLogin, LogOut };
