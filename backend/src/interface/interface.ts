import { type Request } from "express";

interface AuthoRequest extends Request {
  authoEmail: string;
}

export type { AuthoRequest };
