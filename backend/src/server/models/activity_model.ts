import { type RowDataPacket } from "mysql2/promise";
import pool from "./mysqlcon";

const getActivityById = async (activityId: string): Promise<any> => {
  const [activity] = await pool.query(
    `
	  SELECT *
	  FROM ACTIVITY
	  JOIN (
		SELECT ID, GROUP_CONCAT(IMG_PATH) as IMAGES
		FROM ACTIVITY_IMAGE
		GROUP BY ID
	  ) as AI
	  USING(ID) 
	  WHERE ID = ?;
	`,
    activityId
  );
  return activity;
};

const getParticipantsById = async (activityId: string): Promise<any> => {
  const [participants] = await pool.query(
    `
	  SELECT * 
	  FROM REGISTRATION
	  INNER JOIN USER
	  ON REGISTRATION.UEMAIL = USER.UEMAIL
	  WHERE ID = ?
	`,
    activityId
  );
  return participants;
};

const verifyParticipantsRead = async (
  activityId: string,
  email: string
): Promise<boolean> => {
  const [company] = await pool.query(
    "SELECT MGR_UEMAIL FROM ACTIVITY WHERE ID = ?",
    activityId
  );
  if (String((company as RowDataPacket[])[0].MGR_UEMAIL) === email) {
    return true;
  }
  const [staff] = await pool.query(
    "SELECT UEMAIL FROM `LEAD` WHERE ID = ?",
    activityId
  );
  const staffCount = (company as RowDataPacket[]).length;
  for (let i = 0; i < staffCount; ++i) {
    if ((staff as RowDataPacket[])[i].UEMAIL === email) {
      return true;
    }
  }
  return false;
};

const createRegistById = async (
  authoEmail: string,
  activityId: string,
  userInfo: any
): Promise<void> => {
  await pool.query(
    "INSERT INTO REGISTRATION (UEMAIL, RNAME, UPHONE, ID) VALUES (?,?,?,?)",
    [authoEmail, userInfo.name, userInfo.phone, activityId]
  );
};

const updateRegistById = async (
  authoEmail: string,
  activityId: string,
  userInfo: any
): Promise<void> => {
  await pool.query(
    "UPDATE REGISTRATION SET RNAME=?, UPHONE=? WHERE UEMAIL=? AND ID=?",
    [userInfo.name, userInfo.phone, authoEmail, activityId]
  );
};

const verifyRegistrationUpdate = async (
  activityId: string,
  email: string
): Promise<boolean> => {
  const [customer] = await pool.query(
    "SELECT UEMAIL FROM REGISTRATION WHERE ID = ?",
    activityId
  );
  if (String((customer as RowDataPacket[])[0].UEMAIL) === email) {
    return true;
  }
  return false;
};

const getHotActivities = async (): Promise<any> => {
  const [activities] = await pool.query(
    `
	  SELECT ID, ANAME, MIN(IMG_PATH) as COVER 
	  FROM ACTIVITY 
	  JOIN ACTIVITY_IMAGE 
	  USING(ID) 
	  GROUP BY ID 
	  ORDER BY ID DESC
	`
  );
  return activities;
};

const getHeldActivitiesByEmail = async (email: string): Promise<any> => {
  const [activities] = await pool.query(
    `
      SELECT ID, ANAME, MIN(IMG_PATH)as IMAGE
      FROM ACTIVITY
      JOIN ACTIVITY_IMAGE
      USING(ID)
      WHERE MGR_UEMAIL = ?
      GROUP BY ID
    `,
    email
  );
  return activities;
};

const getRegisteredActivitiesByEmail = async (email: string): Promise<any> => {
  const [activities] = await pool.query(
    `
      SELECT ID, ANAME, MIN(IMG_PATH)as IMAGE
      FROM ACTIVITY
      JOIN REGISTRATION
      USING(ID)
      JOIN ACTIVITY_IMAGE
      USING(ID)
      WHERE UEMAIL = ?
      GROUP BY ID
    `,
    email
  );
  return activities;
};

const createActivityByEmail = async (
  activityInfo: any,
  authoEmail: string
): Promise<boolean> => {
  const conn = await pool.getConnection();
  try {
    await conn.query("START TRANSACTION");
    const activityInfoWithMGR = { ...activityInfo, MGR_UEMAIL: authoEmail };
    delete activityInfoWithMGR.IMAGES;
    await conn.query("INSERT INTO ACTIVITY SET ?", activityInfoWithMGR);
    const activityImages = activityInfo.IMAGES.split(",").map(
      (image: string) => [activityInfo.ID, image]
    );
    await conn.query("INSERT INTO ACTIVITY_IMAGE (ID, IMG_PATH) VALUES ?", [
      activityImages
    ]);
    await conn.query("COMMIT");
    return true;
  } catch (error) {
    await conn.query("ROLLBACK");
    console.log(error);
    return false;
  } finally {
    conn.release();
  }
};

function difference(arr1: string[], arr2: string[]): string[] {
  return arr1.filter((element) => !arr2.includes(element));
}

const updateActivityById = async (
  activityInfo: any,
  authoEmail: string
): Promise<boolean> => {
  const conn = await pool.getConnection();
  try {
    const activityInfoWithMGR = { ...activityInfo, MGR_UEMAIL: authoEmail };
    delete activityInfoWithMGR.IMAGES;
    const activityId = activityInfo.ID;
    await conn.query("UPDATE ACTIVITY SET ? WHERE ID = ?", [
      activityInfoWithMGR,
      activityId
    ]);
    const [oldImages] = await conn.query(
      "SELECT IMG_PATH FROM ACTIVITY_IMAGE WHERE ID = ?",
      activityId
    );
    const oldImagesArray = (oldImages as Array<{ IMG_PATH: string }>).map(
      ({ IMG_PATH }: { IMG_PATH: string }) => IMG_PATH
    );
    const newImagesArray = activityInfo.IMAGES.split(",");
    const toDeleteImage = difference(oldImagesArray, newImagesArray);
    if (toDeleteImage.length !== 0) {
      await conn.query(
        "DELETE FROM ACTIVITY_IMAGE WHERE ID = ? AND IMG_PATH IN (?)",
        [activityId, toDeleteImage]
      );
    }
    const toAddImage = difference(newImagesArray, oldImagesArray).map(
      (image: string) => [activityId, image]
    );
    if (toAddImage.length !== 0) {
      await conn.query("INSERT INTO ACTIVITY_IMAGE (ID, IMG_PATH) VALUES ?", [
        toAddImage
      ]);
    }
    return true;
  } catch (error) {
    await conn.query("ROLLBACK");
    console.log(error);
    return false;
  } finally {
    conn.release();
  }
};

export {
  getActivityById,
  getParticipantsById,
  verifyParticipantsRead,
  createRegistById,
  updateRegistById,
  verifyRegistrationUpdate,
  getHotActivities,
  getHeldActivitiesByEmail,
  getRegisteredActivitiesByEmail,
  createActivityByEmail,
  updateActivityById
};
