# 說明

## 安裝

### `npm install --legacy-peer-deps`

加上 `--legacy-peer-deps` 避免相依性問題

## 本地端測試

### `npm start`

啟動 React App

### `npm run test`

跑單元測試(jest)

### `npm run lint`

跑語意檢查(eslint)與風格檢查(prettier)

## 測試覆蓋率

### 前端 `npm test -- --coverage --watchAll=false`

### 後端 `npm test -- --coverage`

## 導入測試資料庫

### `mysql -u <username> -p <database_name> < testdb.sql`

or

### `mysql -u <username> -p`

### `source <full path>/ACMEDLEY/database/testdb.sql;`

## 後端 DEV

- Login: `curl -X POST -H "Content-Type: application/json" -d '{"user_name": <user name>, "user_mail":<user email>}'  http://localhost:8000/account/login`
- Create the Registration of an activity: `curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer <token>" -d '{"name":<name>, "phone":<phone num>, "email":<email>}' http://localhost:8000/api/1.0/activity/register/create\?activityId=<ID>`
- Update the Registration of an activity: `curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer <token>" -d '{"name":<name>, "phone":<phone num>, "email":<email>}' http://localhost:8000/api/1.0/activity/register/update\?activityId=<ID>`
